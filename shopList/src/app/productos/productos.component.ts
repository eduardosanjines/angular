import { Component, OnInit, Input } from '@angular/core';
//import { FormControl } from '@angular/forms';
import { Productos } from '../models/productos.model';


@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  productos: Productos[];

  @Input() required: boolean | string

  constructor() { this.productos = []; }

  ngOnInit(): void {


  }

  addProduct(name: string, price: number, url: string, desc: string): boolean {
    this.productos.push(new Productos(name, price, url, desc));
    console.log(this.productos);
    console.log(name);
    return false;
  }

  deleteProduct(n: string): boolean {
    const found = this.productos.find(element => element.name = n);
    const index = this.productos.indexOf(found, 0);

    if (index > -1) {
      this.productos.splice(index, 1);
    }
    console.log(found);
    console.log(index);
    return true;
  }


 

}
