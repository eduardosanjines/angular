export class Productos {

    name: string;
    price: number;
    url: string;
    desc: string;

    constructor(n: string, p: number, url: string, d: string) {
        this.name = n;
        this.price = p;
        this.url = url;
        this.desc = d;
    }

}